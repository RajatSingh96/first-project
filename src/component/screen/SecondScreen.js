import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, Dimensions, Image, TouchableOpacity } from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign';

const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

const { width, height } = Dimensions.get('window');


const tempArray = [
    { name: "Dr Chintaka Appuhamy", speciality: "Ear nose & Throat Doctor", fees: '500', avatar: require('../../assets/Intersection4.png') },
    { name: "Dr Sruthi AK", speciality: "Ear nose & Throat Doctor ", fees: '600', avatar: require('../../assets/Intersection4.png') },
    { name: "Dr Chintaka Appuhamy", speciality: "Ear nose & Throat Doctor", fees: '500', avatar: require('../../assets/Intersection4.png') },
    { name: "Dr Sruthi AK", speciality: "Ear nose & Throat Doctor ", fees: '600', avatar: require('../../assets/Intersection4.png') },
    { name: "Dr Chintaka Appuhamy", speciality: "Ear nose & Throat Doctor", fees: '500', avatar: require('../../assets/Intersection4.png') },
    { name: "Dr Sruthi AK", speciality: "Ear nose & Throat Doctor ", fees: '600', avatar: require('../../assets/Intersection4.png') },



]

export default class SecondScreen extends Component {
    render() {
        return (
            <ScrollView style={{ width: width, height: height, }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: vh * 2}}>
                    <View style={{ width: vw * 90 }}>
                        {tempArray && tempArray.map((x, i) => {
                            return <View style={{ flexDirection: 'row', marginBottom: vh * 3, borderBottomWidth: 1, borderBottomColor: '#ddd', paddingBottom: 5 }}>
                                <View>
                                    <View>
                                        <Image style={{ width: 70, height: 70, resizeMode: 'contain' }}
                                            source={x.avatar} />
                                    </View>
                                    <View style={{ flexDirection: 'row', marginLeft: vw * 3, marginTop: vh * 1 }}>
                                        <AntDesign name="star" size={16} color="#f67e4f" style={{ marginRight: 5 }} />
                                        <Text style={{ color: '#f67e4f', fontWeight: 'bold' }}>4.9</Text>

                                    </View>

                                </View>
                                <View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <View style={{ marginLeft: vw * 3,marginTop:vh*1.8}}>
                                            <Text style={styles.name}>{x.name}</Text>
                                            <Text>{x.speciality}</Text>
                                        </View>

                                        <View style={{marginTop:vh*2.5}} >
                                            <Text style={{ color: '#00377d', fontWeight: 'bold' }}>₹{x.fees}/-</Text>
                                        </View>

                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: vw * 3, }}>
                                        <TouchableOpacity
                                            style={styles.buttonStyle}
                                            activeOpacity={0.5}
                                            // onPress={this.openFile}
                                            >
                                            <Text style={styles.buttonTextStyle}>
                                                Get Second Opinion
                                        </Text>
                                        </TouchableOpacity>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{ marginLeft: vw * 4, marginTop: vh *3}}>
                                                <Image style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                                    source={require('../../assets/profile.png')} />
                                            </View>
                                            <View style={{ justifyContent: 'center', marginTop: vh * 3, marginLeft: vw * 2 }}><TouchableOpacity>
                                                <Text style={{ color: '#00377d', fontWeight: 'bold' }}>View Profile</Text>
                                            </TouchableOpacity>
                                            </View>

                                        </View>
                                    </View>
                                </View>
                            </View>
                        })}
                    </View>
                </View>
            </ScrollView >
        )
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        backgroundColor: '#00377d',
        borderWidth: 0,
        color: '#00377d',
        borderColor: '#307ecc',
        // height: 30,
        alignItems: 'center',
        borderRadius: 5,
        marginTop: 15,
        // width: '80%',
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        padding: 5,
        fontSize: 13,
        fontWeight: 'bold'
    },
    name: {
        color: '#626262',
        fontWeight: 'bold'
    }
})
