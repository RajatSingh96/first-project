import React, { Component } from 'react'
import { Text, StyleSheet, View, Linking, TouchableOpacity } from 'react-native'


export default class PdfOpener extends Component {

    async openFile() {
        var url = "http://www.africau.edu/images/default/sample.pdf";
        await Linking.openURL(url).catch((err) => {
            console.log(err)
        })
    }


    render() {
        return (
            <View style={styles.mainBody}>
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        activeOpacity={0.5}
                        onPress={this.openFile}>
                        <Text style={styles.buttonTextStyle}>
                            Open File
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
}


const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        justifyContent: 'center',
        padding: 20,
    },
    buttonStyle: {
        backgroundColor: '#307ecc',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#307ecc',
        height: 40,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 15,
        width: '100%',
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
});
