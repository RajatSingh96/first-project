import React, { Component } from 'react'
import { Text, StyleSheet, View, Dimensions, Image, ScrollView } from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const tempArray = [
    { number: "1", details: "Fill your details and Medical History", avatar: require('../../assets/doctors3x.png') },
    { number: "2", details: "Select Your Doctor", avatar: require('../../assets/doctors3x.png') },
    { number: "3", details: "Your Quotes ", avatar: require('../../assets/doctors3x.png') },


]

const { width, height } = Dimensions.get('window');

const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;

export default class FirstScreen extends Component {
    render() {
        return (
            <ScrollView style={{ width: width, height: height, }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: vh * 3 }}>
                    <View style={{ width: vw * 90 }}>
                        <Text style={styles.text}>Access the expertise of our top physician specialist from anywhere in the world.
                         Get our diagnosis reviewed and get educated about your treatment options.</Text>
                        <View style={{ marginTop: vh * 1.4 }}>
                            <Text style={{ color: '#00377d', fontSize: RFPercentage(2.1), fontWeight: 'bold', textAlign: 'justify' }}>You can now get your online second opininon in 3 steps  @ RS...</Text>
                        </View>
                        {tempArray && tempArray.map((x, i) => {
                            return <View style={{ marginTop: vh * 4, flexDirection: 'row',width:vw * 90 }}>
                                <View>
                                    <Image style={{ width: 100, height: 100, resizeMode: 'contain' }}
                                        source={x.avatar} />
                                </View>
                                <View style={styles.heading}>
                                    <View style={{ flexDirection: 'row' ,marginLeft:vw*2}}>
                                        <Text style={styles.number}>{x.number}</Text>
                                        <View style={{ width: 50, borderBottomWidth: 2, borderBottomColor: '#8aafe7' }}></View>
                                    </View>
                                    <View style={{width:'95%',marginLeft:vw*3,marginTop:vh*1.5}}>
                                        <Text style={styles.detail}>{x.details}</Text>
                                    </View>
                                </View>
                            </View>
                        })}



                        <View style={{ marginTop: vh * 3 }}>
                            <Text style={{ lineHeight: 25, color: '#585858', fontSize: RFPercentage(2.1), textAlign: 'justify' }}>
                                Your Doctor will analyze your diagnosis and
                                will get back to you in a week's time through email.
                                if any additional info is required you will be contacted phone
                            </Text>
                        </View>
                        <View style={{ marginTop: vh * 2 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: RFPercentage(2.2), textAlign: 'auto', color: '#273238' }}>
                                Your report can be downloaded on our website too.
                            </Text>

                        </View>
                    </View>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        lineHeight: RFPercentage(3.6),
        textAlign: 'justify',
        fontSize: RFPercentage(2.1),
        color: '#585858'
    },
    number: {
        fontWeight: 'bold',
        fontSize: RFPercentage(2.5),

    },
    detail: {
        fontWeight: 'bold',
        fontSize: RFPercentage(2.2),
        // textAlign: 'center',
        color: '#273238'
    },
    heading: {
        marginTop: vh * 5
    }
})
